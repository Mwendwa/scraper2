var request = require("request-promise");
// require('request-debug')(request);
var cheerio = require("cheerio");
var mkdirp = require("mkdirp");
var extract = require('pdf-text-extract');
var moment = require("moment");
var fs = require("fs");
var iconv = require("iconv-lite");
var results = require("../results");


function search()  {
  var headers = {};
  headers['Host'] ='www.gob.gba.gov.ar';
  headers['User-Agent'] = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.86 Safari/537.36";
  headers['Accept'] ='*/*';
  headers['Accept-Language'] ='en-US,en;q=0.5';
  headers['Content-Type'] ='application/x-www-form-urlencoded; charset=UTF-8';
  headers['X-Requested-With'] ='XMLHttpRequest';
  headers['Referer'] ='http://www.gob.gba.gov.ar/dijl/';
  headers['Connection'] = 'keep-alive';

  var options = {
    method: 'POST',
    uri: 'http://www.gob.gba.gov.ar/dijl/DIJL_buscador.php?tipo=01&T1&T2=ley',
    headers: headers
  };

  request(options)
        .then(function (html) {
          fs.writeFile("./junk/post.html", html);
          var $ = cheerio.load(html);
          results.parseResults(html);


        })
        .catch(function (err) {
            console.log(err);
            return false;
        });
}

search();