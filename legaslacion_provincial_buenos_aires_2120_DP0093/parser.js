var request = require("request-promise");
// require('request-debug')(request);
var cheerio = require("cheerio");
var mkdirp = require("mkdirp");
var extract = require('pdf-text-extract');
var moment = require("moment");
var fs = require("fs");



exports.getDocument = function (link) {
  var url = 'http://www.gob.gba.gov.ar/dijl/' + link + '&ajax=1';
  var num = link.split(/\=/)[1];
  request({
    uri: url,
    "method":"GET"

   }, function (error, response, html) {
    if (!error && response.statusCode == 200) {
      fs.writeFile(num + ".html", html);
      var doc = {};
      doc.affectations = [];
      var $ = cheerio.load(fs.readFileSync('./13.html')),
       docLink = $("p:contains(DIJL_buscaid.php?var)").text(),
       id = docLink.split('=')[1],
       law = $("h2").text(),
       document_type = (law.split(":")[0]).trim(),
       law_number = (law.split(":")[1]).trim(),
       pdfLink = $("a:contains(Texto original en PDF)").attr("href"),
       htmlLink = $("a:contains(Texto de la Norma)").attr("href"),
       fundamentos = $("a:contains(Fundamentos)").attr("href"),
       law_title = $("font").first().text(),
       promulgation_date = ($("font:contains(Promulgación)").text()).split(":")[1].trim(),
       publication_date = ($("font:contains(Publicación)").text()).split(":")[1].trim(),
       table = $("table[class='table']");
      table.each(function(){
        var tds = $(this).find("td"),
          object = {},
          related_number = tds.first().text(),
          related_title = tds.last().text();
        object.related_number = related_number;
        object.related_title = related_title;
        doc.affectations.push(object);
      })

      doc.id = id;
      doc.law_title = law_title;
      doc.fundament = fundamentos
      doc.document_type = document_type;
      doc.law_number = law_number;
      doc.promulgation_date = promulgation_date;
      doc.publication_date = publication_date;

      console.log(doc);
      }
    });
};
