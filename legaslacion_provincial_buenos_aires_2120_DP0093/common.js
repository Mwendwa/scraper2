var fs = require('fs');
var mkdirp = require("mkdirp");
var request = require('request-promise');

exports.project = "DP0093_legislacion_provincial_buenos_aires_2120";

exports.initialize = function () {
    mkdirp(exports.errorDir(), function (err) {
        if (err)
            return console.error("Error creating directory: " + err);
    });
    mkdirp(exports.dataDir(), function (err) {
        if (err)
            return console.error("Error creating directory: " + err);
    });
    mkdirp(exports.binariesDir(), function (err) {
        if (err)
            return console.error("Error creating directory: " + err);
    });
};

exports.downloadFileTo = function (uri, filename, callback) {
    var dirName = filename.substring(0, filename.lastIndexOf("/"));
    mkdirp.sync(dirName);
    console.log("downloading file to " + filename);
    request.head(uri, function (err, res, body) {
        request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
    });
};
exports.tmpDir = function () {
    var tmp;
    if (process.platform === 'win32') {
        tmp = "C:/Users/tindase/Desktop/vLex/tmp/" + exports.project;
    } else {
        tmp = "./exports/" + exports.project;
    }
    return tmp;
};

exports.errorDir = function () {
    return exports.tmpDir() + "/errors";
};
exports.dataDir = function () {
    return exports.tmpDir() + "/json";
};
exports.binariesDir = function () {
    return exports.tmpDir() + "/binaries";
};