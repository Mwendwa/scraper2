var request = require("request-promise");
// require('request-debug')(request);
var cheerio = require("cheerio");
var mkdirp = require("mkdirp");
var extract = require('pdf-text-extract');
var moment = require("moment");
var fs = require("fs");
var parser = require("./parser");

exports.parseResults = function (html) {
  var $ = cheerio.load(html);
  var links = $("a[class='linkLey']");
  links.each(function(){
    var url = $(this).attr('href');
    parser.getDocument(url);
  })
  var nextPage =$("a[class='botonPaginador']").first().text();
  console.log(nextPage);
};
